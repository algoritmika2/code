Установка зависимостей:
```
pip install -r requirements.txt
```


Для старта приложения наберите в консоли:
```
uvicorn main:app --reload
```
